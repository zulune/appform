from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response, HttpResponseRedirect, redirect, get_object_or_404
from django.contrib import messages
from . import models as tform_m
from . import forms as tform_f
from django.contrib import auth

# Create your views here.
from django.template import RequestContext



def blog(request):
	if request.user:
		user = request.user
	blogs_all = tform_m.Blog.objects.all()
	category_all = tform_m.Category.objects.all()
	template_data = {'blogs_all': blogs_all,
					 'user': user,
					 'category_all':category_all}
	return render_to_response('other/blog.html', template_data)

def blogs(request, id):
	if request.user:
		user = request.user
	blogs_all = tform_m.Blog.objects.filter(id=id)
	comments_all = tform_m.Comments.objects.filter(blog_id=id)
	form = tform_f.CreateComments()
	template_data = {'blogs_all': blogs_all,
					 'comments_all': comments_all,
					 'form': form,
					 'user': user}
	return render_to_response('other/see_blog.html', template_data)

def category(request, id):
	if request.user:
		user = request.user
	category_fl = tform_m.Blog.objects.filter(category_id=id)
	template_data = {'user':user, 'category_fl': category_fl}
	return render_to_response('other/category.html', template_data)

def create_blog(request):
	if request.user:
		user = request.user
	if request.POST:
		form = tform_f.Create_blog(request.POST)
		if form.is_valid():
			form.save()
		return HttpResponseRedirect('/')
	else:
		form = tform_f.Create_blog()
		template_data = {'form': form, 'user': user}
	return render_to_response('other/create_blog.html', template_data)

def add_comments(request, blog_id):
	b = tform_m.Blog.objects.get(id=blog_id)
	if request.POST:
		form = tform_f.CreateComments(request.POST)
		if form.is_valid:
			comment = form.save(commit=False)
			comment.blog = b
			comment.save()
		return HttpResponseRedirect('/see_blog/%s/' % blog_id)

def signin(request):
	if request.user:
		user = request.user
	if request.POST:
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = auth.authenticate(username=username, password=password)
		if user is not None:
			auth.login(request, user)
			return HttpResponseRedirect('/')
		else:
			return render_to_response('other/signin.html')
	else:
		template_data = {'user': user}
		return render_to_response('other/signin.html', template_data)

def logout(request):
	auth.logout(request)
	return redirect('/')

def signup(request):
    if request.POST:
        form = tform_f.UserCreationForm(request.POST)
        profile_form = tform_f.UserProfileForm(request.POST)
        if form.is_valid() and profile_form.is_valid():
            form.save()
            user = auth.authenticate(username=form.cleaned_data['username'],
                                     password=form.cleaned_data['password2'])
            auth.login(request, user)
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()
            registered = True
            return HttpResponseRedirect('/')
    else:
        form = tform_f.UserCreationForm()
        profile_form = tform_f.UserProfileForm()
    template_data = {'form': form, 'profile_form': profile_form}
    return render_to_response('other/signup.html', template_data)


def profile_list(request):
    if request.user:
        user = request.user
    data_profile = tform_m.Profile.objects.filter(user_id=request.user.id)
    template_data = {'data_profile': data_profile, 'user': user}
    return render_to_response("other/profile_list.html", template_data,
                          context_instance=RequestContext(request))

def edit_profile(request):
	if request.user:
		user = request.user
	if request.POST:
		form = tform_f.EditProfileForm(request.POST)
		if form.is_valid():
			post_data = form.cleaned_data
			profile = tform_m.Profile.objects.get(user_id=user.id)
			city = form.cleaned_data['city']
			phone = form.cleaned_data['phone']
			return HttpResponseRedirect(reverse('profile_list'))
	else:
		form = tform_f.CreateProfileForm()
	template_data = {'user': user, 'form': form}
	return render_to_response('other/edit_profile.html', template_data, context_instance=RequestContext(request))

def create_profile(request):
	if request.user:
		user = request.user
	if request.POST:
		form = tform_f.CreateProfileForm(request.POST)
		if form.is_valid():
			post_data = form.cleaned_data
			tform_m.Profile.objects.create(**post_data)
			return HttpResponseRedirect(reverse('profile_list'))
	else:
		form = tform_f.CreateProfileForm()
	template_data = {'form': form, 'user': user}
	return render_to_response('other/edit_profile.html', template_data, context_instance=RequestContext(request))