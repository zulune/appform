
__author__ = 'max'
import datetime
from django.conf import settings
from django import forms
from django.utils.safestring import mark_safe
from . import models as tform_m
from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserCreationForm

ERROR_FLAG = {'required': mark_safe('<button class="btn btn-danger2 btn-xs" type="button"><i class="fa fa-fighter-jet"></i></button>')}
################# BLOG ########################

class Create_blog(forms.ModelForm):
    class Meta:
        model = tform_m.Blog
        fields = ('category', 'title', 'body')

class CreateComments(forms.ModelForm):
    class Meta:
        model = tform_m.Comments
        fields = ('name', 'body')
################# END BLOG ####################

################# PROFILE FORM ################
class BaseProfileForm(forms.Form):
    city = forms.CharField(required=True, label='City')
    phone = forms.CharField(required=True, label='Phone')

class ProfileForm(BaseProfileForm):

    def clean(self):
        cleaned_data = super(ProfileForm, self).clean()
        city = cleaned_data.get('city')
        if tform_m.Profile.objects.filter(city=city.exists()):
            msg = mark_safe('<b>already exists</b>')
            self._errors['city']= self.error_class([msg])
            raise forms.ValidationError(msg)
        return cleaned_data

class EditProfileForm(BaseProfileForm):
    pass

class CreateProfileForm(ProfileForm):
    pass

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = tform_m.Profile
        fields = ['city', 'phone']

############# END PROFILE FORM ################

################# USER FORM ###################
'''
class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = tform_m.User
        fields = ('email', 'username')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Password Dont Math')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password2'])
        if commit:
            user.save()
        return user

class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()
    class Meta:
        model = tform_m.User

    def clean_password(self):
        return self.initial['password']
'''
################# END USER FORM ###############

