from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('appform.tform.views',
    # Examples:
    # url(r'^$', 'appform.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'blog', name='blog'),
    url(r'^see_blog/(?P<id>[?a-z=A-Z0-9_.-]*)/$', 'blogs', name='blogs'),
    url(r'^create_blog/', 'create_blog', name='create_blog'),
    url(r'^comment/(?P<blog_id>[?a-z=A-Z0-9_.-]*)/$', 'add_comments', name='add_comments'),
    url(r'^signin/', 'signin', name='signin'),
    url(r'^signup/', 'signup', name='signup'),
    url(r'^logout/', 'logout', name='logout'),
    url(r'^profile_list/', 'profile_list', name='profile_list'),
    url(r'^edit_profile/$', 'edit_profile', name='edit_profile'),
    url(r'^create_profile/$', 'create_profile', name='create_profile'),
    url(r'^category/(?P<id>[?a-z=A-Z0-9_.-]*)/$', 'category', name='category')
    # url(r'^profile/', 'profile', name='profile')1
    # url(r'blog/', 'blog', name='blog')

    # url(r'^user_profile/(?P<id>[?a-z=A-Z0-9_.-]*)/$', 'user_profile', name='user_profile'),
    # url(r'^edit_user_profile/(?P<id>[?a-z=A-Z0-9_.-]*)/$', 'edit_user_profile', name='edit_user_profile')
)