import datetime
from django.contrib.auth.models import User, BaseUserManager, AbstractBaseUser
from django.db import models


# Create your models here.

################## BLOG #########################
class Category(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class Blog(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=50)
    body = models.TextField()
    date = models.DateTimeField(default=datetime.datetime.now(), blank=True)

    def __unicode__(self):
        return self.title

class Comments(models.Model):
    blog = models.ForeignKey(Blog)
    name = models.CharField(max_length=50)
    body = models.TextField()

    def __unicode__(self):
        return self.name

class Profile(models.Model):
    user = models.OneToOneField(User)
    city = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=50, blank=True)

    def __unicode__(self):
        return self.city

'''
################### USER ########################

class UserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            username=username)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(email,
                                password=password,
                                username=username)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='Email',
        max_length=255,
        unique=True,
        db_index=True)
    username = models.CharField(verbose_name='usename',  max_length=255, unique=True)
    first_name = models.CharField(verbose_name='First name',  max_length=255, blank=True)
    last_name = models.CharField(verbose_name='Last name',  max_length=255, blank=True)
    date_of_birth = models.DateField(verbose_name='Birht day',  blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name,)

    def get_short_name(self):
        return self.username

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin
################## END USER #####################
'''



