from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'appform.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'', include('appform.tform.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
