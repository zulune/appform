import os
import sys
import csv
import datetime

DJANGO_CWD = os.getcwd()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'appform.settings')
from django.conf import settings

import django
django.setup()

from appform.tform.models import *
import appform.tform.models as tform_m
from django.contrib.auth.models import User

EXPORTED_FILES_CWD = os.path.join(DJANGO_CWD, 'appform/support_scripts/exported_tables/ap_db_files/')

############### ADD FUNC ############

def add_auth_user():
	try:
		user_1 = User.objects.create_user('black', 'black@gmail.com', 'blac')
		user_1.first_name = 'Black'
		user_1.last_name = 'Jack'
		user_1.save()
	except:
		user_1 = None
	print 'User already exist ...'

	try:
		user_2 = User.objects.create_user('jack', 'jack@gmail.com', 'jack')
		user_2.first_name = 'Jack'
		user_2.last_name = 'Daniels'
		user_2.save()
	except:
		user_2 = None
	print 'Users already exist ...'

	print 'Users created ...'

def add_blog(**values):
	i = Blog.objects.get_or_create(**values)[0]

def add_comment(**values):
	i = Comments.objects.get_or_create(**values)[0]

############## END ADD_FUNCTION ###########

def get_path(first_name):
	''' Get the path of the requested file.
	'''
	return os.path.join(EXPORTED_FILES_CWD, first_name)

############## POPULATE FUNC ##############

def populate_user():
	add_auth_user()

def populate_blog():
	# deleted tables for blog
	Blog.objects.all().delete()
	csv_file = get_path('appform_blog.csv')
	# oped file with data
	with open(csv_file) as f:
		f.seek(0)
		file_csv = csv.reader(f)
		header = next(file_csv)
		for row in file_csv:
			dict_values = dict(zip(header, row))
			add_blog(**dict_values)
	print 'Blog population is finished ...'

def populate_comment():
	Comments.objects.all().delete()
	csv_file = get_path('appform_comments.csv')
	with open(csv_file) as f:
		f.seek(0)
		file_csv = csv.reader(f)
		header = next(file_csv)
		for row in file_csv:
			dict_values = dict(zip(header, row))
			add_comment(**dict_values)
	print 'Comments population is finished ...'

def print_summary():
	print "=================SUMMERY=============================="
	print "Loop through created items"
	print "- {0} - {1} - {2}".format(str(1), str(2), str(3))
	print "=================SUMMERY ENDS=============================="


############ END POPULATE FUNCTION ########

if __name__ == '__main__':
	print 'Starting AppForm population script'
	populate_user()
	populate_blog()
	populate_comment()
