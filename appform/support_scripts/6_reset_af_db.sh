#!/bin/bash

echo "RUN ALL SCRIPTS"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
############### FUNC ##################

function init_globals {
	django_root_loc=$HOME'/US/dev/django/appform'
	echo $django_root_loc
	django_project_loc=$django_root_loc'/appform'
	echo $django_project_loc
	django_app_loc=$django_project_loc'/tform'
	echo $django_app_loc
	script_loc=$django_root_loc'/appform/support_scripts'
	echo $script_loc
}

############### END FUNC ##############

############### execute ###############

init_globals;
echo "Globals are set ..."
# source $script_loc'/4_drop_tables.sh'
echo 'Tables are dropped ....'

mkdir $django_app_loc'/migrations'
sudo chmod -R ugo=rwx $django_app_loc'/migrations'
touch $django_app_loc'/migrations/__init__.py'
echo '__init__.py is created ...'

cd $django_root_loc
python manage.py syncdb
echo 'Superuser is created ...'

sudo rm -rf $django_app_loc'/migrations'
echo '/migrations are removed ...'

python manage.py syncdb
echo 'SyncDB is generated ...'

python < $script_loc'/populate_blog_scripts.py'

#source $script_loc'5_fix_db.sh'
echo 'Are fixed ...'
################# END  ################